const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
const weeks = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
const ids = ['dayname', 'month', 'daynum', 'hour', 'min', 'sec', 'periods']
setInterval(() => {
    var now = new Date()
    dnam = now.getDay()
    mo = now.getMonth()
    dnum = now.getDate()
    hr = now.getHours()
    mi = now.getMinutes()
    se = now.getSeconds()
    pe = "AM"
    if (hr == 0) {
        hr = 12
    } if (hr > 12) {
        hr = hr - 12
        pe = "PM"
    }
    Number.prototype.pad = function (digits) {
        for (var n = this.toString(); n.length < digits; n = 0 + n);
        return n;
    }
    const values = [weeks[dnam], months[mo], dnum, hr, mi.pad(2), se.pad(2), pe]
    for (var i = 0; i < ids.length; i++)
        document.getElementById(ids[i]).firstChild.nodeValue = values[i]

}, 1000);
const loc = document.getElementById('loc')
const ico = document.getElementById('icon')
const climate = document.getElementById('climate')
const temprature = document.getElementById('temp')
const hum = document.getElementById('humidity')
const pres = document.getElementById('Pressure')
const wind = document.getElementById('wind')


fetch('https://api.openweathermap.org/data/2.5/weather?q=kozhikode,india&units=metric&appid=70247b8269f1c8d48145301187a652cc')
    .then(response => {
        return response.json()
    }).then(data => {
        console.log(data)
        const { name } = data
        const { feels_like, humidity, pressure } = data.main
        const { main, icon } = data.weather[0]
        const { speed } = data.wind
        loc.textContent = name
        temprature.textContent = "Temprature : " + feels_like + "°C"
        hum.textContent = "Humidity : " + humidity + "%"
        pres.textContent = "Pressure : " + pressure + " mb"
        climate.textContent = main
        wind.textContent = "Wind speed : " + speed + "km/h"
        ico.src = " http://openweathermap.org/img/wn/" + icon + "@2x.png"

    })
const btn = document.getElementById('btn1')
const input = document.getElementById('inp')
const place = document.getElementById('place')
const symbol = document.getElementById('ico')
const clim = document.getElementById('clim')
const feel = document.getElementById('tem')
const hy = document.getElementById('humid')
const pr = document.getElementById('Press')
const wd = document.getElementById('win')

btn.addEventListener('click', () => {
    input.innerHTML = ""
    fetch('https://api.openweathermap.org/data/2.5/weather?q=' + input.value + '&units=metric&appid=70247b8269f1c8d48145301187a652cc')
        .then(res => {
            return res.json()
        }).then(dat => {
            console.log(dat)
            const { name } = dat
            const { feels_like, humidity, pressure } = dat.main
            const { main, icon } = dat.weather[0]
            const { speed } = dat.wind
            place.textContent = name
            symbol.src = " http://openweathermap.org/img/wn/" + icon + "@2x.png"
            feel.textContent = "Temprature : " + feels_like + "°C"
            hy.textContent = "Humidity : " + humidity + "%"
            pr.textContent = "Pressure : " + pressure + " mb"
            clim.textContent = main
            wd.textContent = "Wind speed : " + speed + "km/h"



        }).catch(err => {
            console.log("error")
            alert("Please enter valid place!")
        })
})